package service

import (
	"context"
	"fmt"
	"log"
	"strings"
	"time"
)

type RepoExtractorService interface {
	RefreshRepositories(ctx context.Context, candidateId string) error
	ExtractRepositories(ctx context.Context, baseUrl string, provider croauth.Provider, userId string, tokenStore *oauth2.Token, commitTime string) (string, error)
}

func main() {
    fmt.Println("Test")
}
